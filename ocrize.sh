#!/bin/bash

CONFIG_FILE="${HOME}/.config/ocrize/ocrize.conf"

function _error_exit {
    echo "ERROR: $*" > /dev/stderr
    exit 1
}

function _create_nx_folder {
    if [ $# -eq 1 ]; then
        file_name=$(basename "$1")
        file_dir="${1%${file_name}}"
        mkdir -p "${file_dir}"
    else
        echo "ERROR: called $0 with the wrong number of arguments (provided args: [$*])" > /dev/stderr
    fi
}

function _init {
    # shellcheck disable=SC2016
    [[ $(type -P "sqlite3") ]] || _error_exit '"sqlite3" is not on your $PATH'
    # shellcheck disable=SC2016
    [[ $(type -P "tesseract") ]] || _error_exit '"tesseract" is not on your $PATH'
    # shellcheck disable=SC2016
    [[ $(type -P "convert") ]] || _error_exit '"ImageMagick" is not on your $PATH'
    # shellcheck disable=SC2016
    [[ $(type -P "md5sum") ]] || _error_exit '"md5sum" is not on your $PATH'

    _create_nx_folder "${CONFIG_FILE}"

    if [ ! -f "${CONFIG_FILE}" ]; then
        # shellcheck disable=SC2016
        echo -e 'ocrize_db_file="$HOME/.local/share/ocrize/ocrize.db"\nocrize_langs=("eng")\nocrize_dirs_eng=("$HOME/Documents")' > "${CONFIG_FILE}"
        echo "Created default config file at ${CONFIG_FILE}" > /dev/stderr
        exit 0
    fi

    PNG_DIR="$(mktemp -d -t ocrize_XXXX)"
}

function _init_db {
    _create_nx_folder "${ocrize_db_file}"
    echo -e 'Create Table Ocrize(\n    FilePath Text Primary Key Not Null,\n    Md5 Char(32) Not Null,\n    FullText Text,\n    FileExt Char(16),\n    DatetimeAdded Int Not Null\n);\n' | sqlite3 "${ocrize_db_file}"
    echo "Initialized empty DB at ${ocrize_db_file}" > /dev/stderr
}

function _read_conf {
    # shellcheck source=/home/alfred/.config/ocrize/ocrize.conf
    . "${CONFIG_FILE}"
    if [ ! -f "${ocrize_db_file}" ]; then
        _init_db
    fi

    T_LANGS=$(tesseract --list-langs)
    for lang in "${ocrize_langs[@]}"; do
        echo "${T_LANGS}" | grep -qE "^${lang}$" || _error_exit "Language '${lang}' is not available in tesseract"
    done
}

function _dump_pdf_contents {
    rm -f "${PNG_DIR}/output*.png"
    if [ $# -eq 2 ] && [ -f "$2" ]; then
        convert -density 150 "$2" -quality 90 "${PNG_DIR}/output.png"
        for page in "${PNG_DIR}/"output*.png; do
            tesseract -l "$1" "${page}" -
        done | grep -vE '^\s*$' | tr \\n \  | sed -e "s/['|]//g"
        echo ""
    fi 2>/dev/null
}

function _import_file {
    # TODO | only import if md5 changed
    if [ $# -eq 2 ]; then
        md5=$(md5sum "${2}" | cut -d\  -f1)
        file_name=$(basename "${2}")
        file_ext=$(basename "${2}" | sed -e 's/.*\.\([^\.]*\)/\1/g')
        txt=$(_dump_pdf_contents "${1}" "${2}")
        sqlite3 "${ocrize_db_file}" "Delete From Ocrize Where FilePath = '${2}';"
        sqlite3 "${ocrize_db_file}" "Insert Into Ocrize (FilePath, Md5, FullText, FileExt, DatetimeAdded) Values ('${2}', '${md5}', '${txt}', '${file_ext}', datetime('now', 'localtime'));"
        echo "    Imported ${file_name}"
    else
        echo "ERROR: called $0 with the wrong number of arguments (provided args: [$*])" > /dev/stderr
    fi
}

function _scan_lang_dir {
    if [ $# -eq 2 ]; then
        if [ -d "${2}" ]; then
            echo "  Processing directory '${2}'..."
            mapfile -t pdf_files < <(find "${2}" -type f -iname "*.pdf")
            for cur_pdf in "${pdf_files[@]}"; do
                _import_file "$1" "${cur_pdf}"
            done
        fi
    else
        echo "ERROR: called $0 with the wrong number of arguments (provided args: [$*])" > /dev/stderr
    fi
}

function _full_library_scan {
    for lang in "${ocrize_langs[@]}"; do
        cur_dirs=( "ocrize_dirs_${lang}"[@] )
        echo "Processing directories for language '${lang}'..."
        for cur_dir in "${!cur_dirs}"; do
            _scan_lang_dir "$lang" "$cur_dir"
        done
    done
    rm -rf "${PNG_DIR}"
}

function _search {
    # TODO | search all words instead of exact expression
    if [ $# -gt 0 ]; then
        results=$(sqlite3 "${ocrize_db_file}" "Select FilePath, FullText From Ocrize Where FullText Like '%${*}%';")
        if [ -n "${results}" ]; then
            echo -e "${results}" | while read -r result; do
                file_path=$(echo "${result}" | cut -d\| -f1)
                full_text=$(echo "${result}" | cut -d\| -f2)
                excerpt=$(echo -e "${full_text}" | grep -oE ".{0,$C}$*.{0,$C}" | sed -e "s/\(.*\)\($*\)\(.*\)/\1\\\e[4m\2\\\e[0m\3/g")
                echo -e "${file_path} | \"...${excerpt}...\""
            done
        fi
    else
        echo "ERROR: called $0 with the wrong number of arguments (provided args: [$*])" > /dev/stderr
    fi
}

_init
_read_conf
C="${ocrize_context_chars}"
if [ $# -ne 0 ]; then
    _search "$*"
else
    _full_library_scan
fi
