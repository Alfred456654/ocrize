# OCRize

Use Tesseract to read all your scanned documents and store the results in an SQLite database.

PDF files are converted to PNG files by ImageMagick in a temporary directory before being processed by Tesseract.

OCRize can also be used to search documents based on their content.

## Prerequisites

* ImageMagick
* SQLite 3
* Tesseract with at least 1 language pack
* md5sum

## Configuration

A sample configuration file is created at the first execution in `$HOME/.config/ocrize/ocrize.conf`. It is a bash file, so make sure to respect the bash syntax for variable assignment.

There, you can choose:

* `ocrize_db_file`: location of the SQLite database file
* `ocrize_langs`: list of languages for Tesseract (i.e. `ocrize_langs=("eng" "rus" "spa")`)
* `ocrize_context_chars`: number of context characters in search results

For each language specified in `ocrize_langs`, there should be a section with the language trigram:

* `ocrize_dirs_eng`: list of directories that contain documents written in english that you want to parse and index with OCRize

## Usage

### Scan full library

Once you've edited the configuration file to your needs, you can launch `ocrize.sh` without arguments to have it parse and index all your configured folders.

### Search for documents

If you want to look for documents that contain any multi-word expression, just give them as arguments to `OCRize`:

```
$ ./ocrize.sh expression to search
/absolute/path/to/matching/file.pdf | "... this file contains __expression to search__ and some context ..."
```
